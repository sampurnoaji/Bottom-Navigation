package com.example.petersam.btnkw;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;

import adapter.GridViewImageAdapter;
import helper.AppConstant;
import helper.Utils;

public class ProfileFragment extends Fragment {
    
    private GridView gridView;
    private Utils utils;
    private int columnWidth;
    Activity activity;
    Context context;
    private ArrayList<String> imagePaths = new ArrayList<String>();
    private GridViewImageAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        context = getActivity();
        activity = getActivity();
        gridView = view.findViewById(R.id.gv);
        utils = new Utils(context);
        
        //initilizing grid view
        InitilizingGridLayout();

        //loading all image paths from sd card
        imagePaths = utils.getFilePaths();

        //gridview adapter
        adapter = new GridViewImageAdapter(activity, imagePaths, columnWidth);

        //setting grid view adapter
        gridView.setAdapter(adapter);

        return view;
    }

    private void InitilizingGridLayout() {
        Resources resources = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                AppConstant.GRID_PADDING, resources.getDisplayMetrics());
        columnWidth = (int)((utils.getScreenWidth()-((AppConstant.NUM_OF_COLUMNS + 1) * padding))
                / AppConstant.NUM_OF_COLUMNS);

        gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int)padding, (int)padding, (int)padding, (int)padding);
        gridView.setHorizontalSpacing((int)padding);
        gridView.setVerticalSpacing((int)padding);
    }
}
