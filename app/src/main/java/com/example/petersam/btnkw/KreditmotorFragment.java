package com.example.petersam.btnkw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class KreditmotorFragment extends Fragment {

    private android.support.v7.app.ActionBar toolbar;

    EditText et_hargaMotor, et_bunga, et_uangMuka, et_lamaCicilan;
    //Button btn_kalkulasi;
    CardView btn_kalkulasi;
    String shargaMotor, sbunga, suangMuka, slamaCicilan,
        stotalBunga, sangsuran;
    Double dhargaMotor, dbunga, duangMuka, dlamaCicilan,
        dtotalBunga, dangsuran;
    TextView tv_totalBunga, tv_angsuran;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_kreditmotor, container, false);

        et_hargaMotor = view.findViewById(R.id.et_hargaMotor);
        et_bunga = view.findViewById(R.id.et_bunga);
        et_uangMuka = view.findViewById(R.id.et_uangMuka);
        et_lamaCicilan = view.findViewById(R.id.et_lamaCicilan);
        tv_totalBunga = view.findViewById(R.id.tv_totalBunga);
        tv_angsuran = view.findViewById(R.id.tv_angsuranPerBulan);
        btn_kalkulasi = view.findViewById(R.id.btn_kalkulasi);

        btn_kalkulasi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                kalkulasi();
            }
        });
        return view;
    }

    private void kalkulasi() {
        //format penulisan rupiah
        DecimalFormat kursIDR = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIDR.setDecimalFormatSymbols(formatRp);

        shargaMotor = et_hargaMotor.getText().toString();
        dhargaMotor = Double.parseDouble(shargaMotor);

        sbunga = et_bunga.getText().toString();
        dbunga = Double.parseDouble(sbunga);

        suangMuka = et_uangMuka.getText().toString();
        duangMuka = Double.parseDouble(suangMuka);

        slamaCicilan = et_lamaCicilan.getText().toString();
        dlamaCicilan = Double.parseDouble(slamaCicilan);

        dtotalBunga = (dhargaMotor - duangMuka)*(dbunga/100)*(dlamaCicilan);
        stotalBunga = kursIDR.format(dtotalBunga);
        tv_totalBunga.setText(stotalBunga);

        dangsuran = (dhargaMotor - duangMuka + dtotalBunga)/(dlamaCicilan);
        sangsuran = kursIDR.format(dangsuran);
        tv_angsuran.setText(sangsuran);
    }
}
