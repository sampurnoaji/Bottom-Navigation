package com.example.petersam.btnkw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class TabunganFragment extends Fragment {

    EditText et_duit, et_bulan, et_bunga;
    TextView tv_tabunganAkhir;
    //Button b_hitung;
    CardView b_hitung;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_tabungan, container, false);

        et_duit = (EditText) view.findViewById(R.id.et_TabunganAwal);
        et_bulan = (EditText) view.findViewById(R.id.et_Bulan);
        et_bunga = view.findViewById(R.id.et_bunga);
        tv_tabunganAkhir = (TextView) view.findViewById(R.id.tv_TabunganAkhir);
        b_hitung = view.findViewById(R.id.b_hitung);
        b_hitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //format penulisan rupiah
                DecimalFormat kursIDR = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
                formatRp.setCurrencySymbol("Rp ");
                formatRp.setMonetaryDecimalSeparator(',');
                formatRp.setGroupingSeparator('.');
                kursIDR.setDecimalFormatSymbols(formatRp);

                String saldoAwal = et_duit.getText().toString();
                String bulan = et_bulan.getText().toString();
                String bunga = et_bunga.getText().toString();
                Double dbunga = Double.parseDouble(bunga);
                Double dsaldoAwal = Double.parseDouble(saldoAwal);
                Double dbulan = Double.parseDouble(bulan);
                Double dsaldoAkhir = (dsaldoAwal*(dbunga/100)*dbulan) + dsaldoAwal;
                String ssaldoAkhir = kursIDR.format(dsaldoAkhir);
                tv_tabunganAkhir.setText(ssaldoAkhir);
            }
        });
        return view;
    }
}
