package com.example.petersam.btnkw;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    private android.support.v7.app.ActionBar toolbar;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        toolbar.setTitle("Tabungan");
        toolbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D81B60")));
        loadFragment(new TabunganFragment());

        bottomNavigationView = findViewById(R.id.bn_menu);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

       // getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, new TabunganFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                    switch (menuItem.getItemId()){
                        case R.id.navigation_tabungan:
                            toolbar.setTitle("Tabungan");
                            fragment = new TabunganFragment();
                            loadFragment(fragment);
                            break;
                        case R.id.navigation_kreditmotor:
                            toolbar.setTitle("Kredit Motor");
                            fragment = new KreditmotorFragment();
                            loadFragment(fragment);
                            break;
                        case R.id.navigation_notification:
                            toolbar.setTitle("Notifications");
                            fragment = new NotificationFragment();
                            loadFragment(fragment);
                            break;
                        case R.id.navigation_profile:
                            toolbar.setTitle("Profile");
                            fragment = new ProfileFragment();
                            loadFragment(fragment);
                            break;
                        case R.id.navigation_search:
                            toolbar.setTitle("Search");
                            fragment = new SearchFragment();
                            loadFragment(fragment);
                            break;
                    }
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, selectedFragment).commit();
                    return true;
                }
            };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
