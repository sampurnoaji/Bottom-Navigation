package helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

public class Utils {
    private Context _context;

    //constructor
    public Utils(Context context) {

        this._context = context;
    }

    //reading file paths from SDcard
    public ArrayList<String> getFilePaths() {
        ArrayList<String> filePaths = new ArrayList<String>();
        //File directory = new File(
        //        android.os.Environment.getExternalStorageDirectory()
        //                + File.separator + AppConstant.PHOTO_ALBUM);

        String path = android.os.Environment.getExternalStorageDirectory().toString()
                +"/coba";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();

        //check for directory
        if (directory.isDirectory()) {
            //getting list of file paths
            //File[] listfiles = directory.listFiles();
            if (files == null) {

            } else {
                if (files.length > 0) {
                    //loop through all files
                    for (int i = 0; i < files.length; i++) {
                        //get file paths
                        String filePath = files[i].getAbsolutePath();
                        //check for supported file extension
                        if (IsSupportedFile(filePath)) {
                            //add image path to array list
                            filePaths.add(filePath);
                        }
                    }
                } else {
                    //image directory is empty
                    Toast.makeText(_context, AppConstant.PHOTO_ALBUM + "folder kosong",
                            Toast.LENGTH_LONG).show();
                }
            }

            //check for count

        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(_context);
            alert.setTitle("Error bro");
            alert.setMessage(AppConstant.PHOTO_ALBUM + "direktori tidak valid, set namanya di AppConstant.java");
            alert.setPositiveButton("OK", null);
            alert.show();
        }
        return filePaths;

    }

    private boolean IsSupportedFile(String filePath) {
        String ext = filePath.substring(filePath.lastIndexOf(".") + filePath.length());
        if (AppConstant.FILE_EXTN.contains(ext.toLowerCase(Locale.getDefault())))
            return true;
        else
            return false;
    }

    //getting screen width
    public int getScreenWidth() {
        int columnWidth;
        WindowManager windowManager = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }
}
